import { EffectComposer } from 'three/addons/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/addons/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/addons/postprocessing/ShaderPass.js';
import { UnrealBloomPass } from 'three/addons/postprocessing/UnrealBloomPass.js';
import { ChromaticShader } from './ChromaticPass.js';
import { CrtShader } from './CrtPass.js';
import { GammaCorrectionShader } from 'three/addons/shaders/GammaCorrectionShader.js';
import { GlitchPass } from './GlitchPass.js';

var renderer;
var camera;
var scene;
var composer;
var renderPass;
var bloomPass;
var gammaPass;
var crtShader;
var glitchPass;



function initPostProcess(webglRenderer, cam, sc) {
    renderer = webglRenderer;
    camera = cam;
    scene = sc;

    renderPass = new RenderPass(scene, camera);
    bloomPass = new UnrealBloomPass();
    bloomPass.copyUniforms.opacity.value = 0.307;
    crtShader = CrtShader;
    gammaPass = new ShaderPass(GammaCorrectionShader);
    gammaPass.renderToScreen  = true;
    glitchPass = new GlitchPass();

    onPostProcessChanged();
}

function onPostProcessChanged() {
    composer = new EffectComposer(renderer);
    composer.addPass(renderPass);
    composer.addPass(new ShaderPass(ChromaticShader));
    composer.addPass(bloomPass);
    composer.addPass(new ShaderPass(crtShader));
    composer.addPass(glitchPass);
    composer.addPass(gammaPass);
}

function render() {
    composer?.render();
}

export { render, initPostProcess, onPostProcessChanged, glitchPass };
