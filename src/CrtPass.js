let CrtShader = {
    uniforms: {
        tDiffuse: {
            value: null
        },
        curvature: {
            value: [7, 7]
        },
        screenResolution: {
            value: [220, 430]
        },
        scanLineOpacity: {
            value: [0.02, 0.2]
        },
        vignetteOpacity: {
            value: 0.45
        }
    },
    vertexShader:
    /* glsl */
    `
        varying vec2 vUv;

        void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }
        
    `,
    fragmentShader:
    /* glsl */
    `
        varying vec2 vUv;
        uniform sampler2D tDiffuse;
        
        uniform vec2 curvature;
        uniform vec2 screenResolution;
        uniform vec2 screenSize;
        uniform vec2 scanLineOpacity;
        uniform float vignetteOpacity;

        vec4 scanLineIntensity(float uv, float resolution, float opacity)
        {
            float intensity = sin(uv * resolution * 3.14 * 2.0);
            intensity = ((0.5 * intensity) + 0.5) * 0.9 + 0.1;
            return vec4(vec3(pow(intensity, opacity)), 1.0);
        }
    
        vec4 vignetteIntensity(vec2 uv, vec2 resolution, float opacity)
        {
            float intensity = uv.x * uv.y * (1.0 - uv.x) * (1.0 - uv.y);
            return vec4(vec3(clamp(pow((resolution.x / 4.0) * intensity, opacity), 0.0, 1.0)), 1.0);
        }
        
        vec2 curveRemapUV(vec2 uv)
        {
            uv = uv * 2.0 - 1.0;
            vec2 offset = abs(uv.yx) / vec2(curvature.x, curvature.y);
            uv = uv + uv * offset * offset;
            uv = uv * 0.5 + 0.5;
            return uv;
        }
        
        void main(void) 
        {
            vec2 remappedUV = curveRemapUV(vUv);
            vec4 baseColor = texture(tDiffuse, vUv);

            baseColor *= scanLineIntensity(remappedUV.x, screenResolution.y, scanLineOpacity.x);
            baseColor *= scanLineIntensity(remappedUV.y, screenResolution.x, scanLineOpacity.y);
            
            baseColor *= vignetteIntensity(remappedUV, screenResolution, vignetteOpacity);

            if (remappedUV.x < 0.0 || remappedUV.y < 0.0 || remappedUV.x > 1.0 || remappedUV.y > 1.0){
                gl_FragColor = vec4(0.0, 0.0, 0.0, 1.0);
            } else {
                gl_FragColor = baseColor;
            }
        }
    `
};

export { CrtShader }
