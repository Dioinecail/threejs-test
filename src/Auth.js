const urlAddUser = 'https://us-east-1.aws.data.mongodb-api.com/app/application-0-qvaub/endpoint/users/add';
const userIdKey = 'staticos-user-id';

var userId;

function tryAddUser(newUserName) {
    fetch(urlAddUser, {
        cache: "no-cache",
        method: "POST",
        body: `{ "name": "${newUserName}" }`,
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    }).then((res) => {
        return res.json();
    }).then((data) => {
        if (data.result == 405) {
            console.log(`[StaticOS] User with the name: '${newUserName}' already created`);

            let userKey = `${userIdKey}-${newUserName}`;

            userId = localStorage.getItem(userKey);

            if (userId) {
                console.log("User already logged in here");
            }
            else {
                userId = data.userId;
                localStorage.setItem(userKey, userId);
                console.log("Successfully added local user");
            }
        }
        else if (data.result == 200) {
            console.log(`[StaticOS] Added new user with the name: ${newUserName}`);
            let userKey = `${userIdKey}-${newUserName}`;

            userId = data.result.id;

            localStorage.setItem(userKey, userId);
            console.log("Successfully added local user");
        }
        else {
            console.log('[StaticOS] Error: ');
            console.log({ data });
        }
    });
}

export { tryAddUser, userId }