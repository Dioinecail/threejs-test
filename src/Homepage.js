import * as THREE from 'three';
import { camera, cameraSize, cameraSizeX } from './main.js';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import * as Links from './content/Links.js';
import * as Blog from './content/Blog.js';
import * as Music from './content/Music.js';
import * as Portfolio from './content/Portfolio.js';

const isDevMode = true;
const platform = isDevMode ? 'dev' : 'master';

const blogsRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/blog/`;
const artRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/art/`;
const musicRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/music/`;
const linksRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/links/`;
const portfolioRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/portfolio/`;
const friendsRootUrl = `https://raw.githubusercontent.com/Dioinecail/staticOS-content/${platform}/friends/`;

const homepageTimers = {
    introAnim: new THREE.Clock()
};

const raycaster = new THREE.Raycaster();

const settings = {
    minScale: 0.10094,
    maxScale: 1.0829,
    minOffset: 1.3,
    maxOffset: 1.4,
    hoverColor: { r: 0.0, g: 1.0, b: 1.0 },
    followSpeed: 3.0992,
    scaleSpeed: 5.662,
    originOffset: 1.1055,
    relaxedOffset: 1.8,
    followOffset: 0.495,
    test: 0.1,
    buttonRootInitialRotation: -40,
    buttonRootRotation: 25,
    buttonHomepageOffset: -450
};

let isActive = false;
let isMode = {
    set: (value) => {
        isModeProp = value;
    },
    get: () => {
        return isModeProp;
    }
};

let isModeProp;
let homepage;
let homepageItems = {
    tab_art: {
        labelOffset: new THREE.Vector3(-0.04, 0.0, -0.2),
        offsetDirection: new THREE.Vector3(-1.0, 0.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Blog,
        id: 'tab_art',
        name: 'ART',
        url: artRootUrl
    },
    tab_blog: {
        labelOffset: new THREE.Vector3(-0.04, 0.0, -0.35),
        offsetDirection: new THREE.Vector3(1.0, 0.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Blog,
        id: 'tab_blog',
        name: 'BLOG',
        url: blogsRootUrl
    },
    tab_music: {
        labelOffset: new THREE.Vector3(-0.4, 0.0, 0.04),
        offsetDirection: new THREE.Vector3(0.0, 1.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Music,
        id: 'tab_music',
        name: 'MUSIC',
        url: musicRootUrl
    },
    tab_reviews: {
        labelOffset: new THREE.Vector3(-0.3, 0.0, 0.07),
        offsetDirection: new THREE.Vector3(0.0, -1.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Links,
        id: 'tab_links',
        name: 'LINKS',
        url: linksRootUrl
    },
    tab_portfolio: {
        labelOffset: new THREE.Vector3(-0.3, 0.0, 0.07),
        offsetDirection: new THREE.Vector3(0.0, -1.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Portfolio,
        id: 'tab_portfolio',
        name: 'PORTFOLIO',
        url: portfolioRootUrl
    },
    tab_friends: {
        labelOffset: new THREE.Vector3(-0.3, 0.0, 0.07),
        offsetDirection: new THREE.Vector3(0.0, -1.0, 0.0),
        target: null,
        targetPos: null,
        targetScale: true,
        targetTab: Blog,
        id: 'tab_friends',
        name: 'FRIENDS',
        url: friendsRootUrl
    }
};

let interactions = {

}

let font;

let homeItemsPositions = {
    art: null,
    reviews: null,
    music: null,
    blog: null,
    topleft: null,
    topright: null,
    bottomleft: null,
    bottomright: null
};

let materials = [
    new THREE.MeshBasicMaterial({ color: 0xE135ED }), // front
    new THREE.MeshBasicMaterial({ color: 0xE135ED }) // side
];

let hitMaterials = [
    new THREE.MeshBasicMaterial({ color: 0x00FFFF }), // front
    new THREE.MeshBasicMaterial({ color: 0x00FFFF }) // side
];

const debugMaterial = new THREE.LineBasicMaterial({ color: 0x0000ff });
let lastInteraction = null;
let debugLineGeo = null;
let line = null;
const testEvent = new Event('test');



function setRoot(homeRoot) {
    homepage = homeRoot;

    // let anchors = homepage.children.find((a, b, c) => {
    //     return a.name.includes('anchors');
    // });

    // anchors.children.forEach((elem) => {
    //     let id = elem.name.split('_')[1];
    //     let worldPos = new THREE.Vector3();

    //     elem.getWorldPosition(worldPos);

    //     homeItemsPositions[id] = worldPos;

    //     let tabId = `tab_${id}`;

    //     if (tabId in homepageItems) {
    //         homepageItems[tabId].originPos = elem.position;
    //     }
    // });

    createDebugHoverArea();

    document.addEventListener('mousemove', handleMouseMove);
    document.addEventListener('click', handleMouseClick);
    document.addEventListener('keydown', handleKeyDown)
}

function createHomepageButtons() {
    let homepageRoot = document.querySelector('.homepage-root');
    let index = 0;

    Object.keys(homepageItems).forEach(element => {
        // let item = homepage.children.find((a, b, c) => {
        //     return a.name == element;
        // });

        // homepageItems[element].target = item;

        createTextV2(homepageItems[element], index, homepageRoot);

        index++;
    });
}

function setIsActive(state) {
    isActive = state;

    if (state)
        homepageTimers.introAnim.start();
}

function setFont(fontAsset) {
    font = fontAsset;
}

function createText(parent) {

    let id = parent.id.split('_')[1];
    let processed = String(id);

    if (id == 'art') {
        processed = 'a\nr\nt';
    }

    if (id == 'blog') {
        processed = 'b\nl\no\ng';
    }

    let textGeo = new TextGeometry(processed, {
        font: font,
        size: 0.14,
        height: 0.1,
        curveSegments: 8
    });

    textGeo.computeBoundingBox();

    let textMesh = new THREE.Mesh(textGeo, materials);

    textMesh.setRotationFromEuler(new THREE.Euler(-THREE.MathUtils.DEG2RAD * 90.0, 0, 0));

    parent.target.add(textMesh);

    interactions[parent.target.name] = {
        target: parent.target,
        text: textMesh
    };

    textMesh.position.x = parent.labelOffset.x;
    textMesh.position.y = parent.labelOffset.y;
    textMesh.position.z = parent.labelOffset.z;
}

function createTextV2(categoryObject, index, homepage) {
    let rootDiv = document.createElement('div');
    let buttonDiv = document.createElement('button');
    let buttonVfx = document.createElement('div');
    let buttonText = document.createElement('div');

    homepage.appendChild(rootDiv);
    rootDiv.appendChild(buttonDiv);
    buttonDiv.appendChild(buttonVfx);
    buttonDiv.appendChild(buttonText)
    buttonText.textContent = categoryObject.name;

    rootDiv.classList.add('homepage-button-root');
    buttonDiv.classList.add('homepage-button');
    buttonVfx.classList.add('homepage-button-vfx');
    buttonText.classList.add('homepage-button-text');

    let translate = `translate(-50%) rotate(${settings.buttonRootInitialRotation + index * settings.buttonRootRotation}deg)`;

    rootDiv.style.transform = translate;

    buttonDiv.addEventListener('click', (evt) => {
        handleHomepageButtonClicked(categoryObject);
    });
}

function createDebugHoverArea() {

    if (line !== null) {
        homepage.remove(line);
        line.geometry.dispose();
    }

    let offset = isMode.get() ? settings.maxOffset : settings.minOffset;

    let topleft = new THREE.Vector3(-offset, -offset, 0);
    let topRight = new THREE.Vector3(offset, -offset, 0);
    let bottomLeft = new THREE.Vector3(-offset, offset, 0);
    let bottomRight = new THREE.Vector3(offset, offset, 0);

    let points = [];

    points.push(topleft);
    points.push(topRight);
    points.push(bottomRight);
    points.push(bottomLeft);
    points.push(topleft);

    debugLineGeo = new THREE.BufferGeometry().setFromPoints(points);
    line = new THREE.Line(debugLineGeo, debugMaterial);

    homepage.add(line);
}

function update() {
    if (!isActive)
        return;

    let delta = homepageTimers.introAnim.getDelta();

    Object.keys(homepageItems).forEach((id) => {
        let tab = homepageItems[id];
        let item = homepageItems[id].target;

        THREE.Object3D

        if (item !== null) {
            let id = item.name.split('_')[1];

            let p0 = item.position;
            let p1 = tab.targetPos ?? homeItemsPositions[id];

            let s0 = item.scale;
            let s1 = tab.targetScale
                ? new THREE.Vector3(settings.maxScale, settings.maxScale, settings.maxScale)
                : new THREE.Vector3(settings.minScale, settings.minScale, settings.minScale);

            let t = THREE.MathUtils.clamp(delta * settings.followSpeed, 0.0, 1.0);
            let t2 = THREE.MathUtils.clamp(delta * settings.scaleSpeed, 0.0, 1.0);

            let x = THREE.MathUtils.lerp(p0.x, p1.x, t);
            let y = THREE.MathUtils.lerp(p0.y, p1.y, t);
            let z = THREE.MathUtils.lerp(p0.z, p1.z, t);

            let sx = THREE.MathUtils.lerp(s0.x, s1.x, t2);
            let sy = THREE.MathUtils.lerp(s0.y, s1.y, t2);
            let sz = THREE.MathUtils.lerp(s0.z, s1.z, t2);

            item.position.set(x, y, z);
            item.scale.set(sx, sy, sz);
        }
    });
}

function showHomepage() {
    document.querySelector('.homepage-root').style.display = '';
    document.querySelector('.homepage-root').style.pointerEvents = '';

    document.querySelectorAll('.homepage-button').forEach(b => {
        b.classList.remove('is-active');
    });
}

function handleMouseMove(event) {
    handleCursorBounds(event);

    handleHover(event);
}

function handleHover(event) {
    let x = (event.clientX / window.innerWidth) * 2 - 1;
    let y = -(event.clientY / window.innerHeight) * 2 + 1;

    raycaster.setFromCamera({ x: x, y: y }, camera);

    let targets = [];

    Object.keys(interactions).forEach(key => {
        targets.push(interactions[key].target);
    });

    let intersects = raycaster.intersectObjects(targets);

    if (intersects.length > 0) {
        let hit = intersects[0];

        if (hit.object.name in interactions) {
            let hitText = interactions[hit.object.name];

            hitText.text.material = hitMaterials;

            if (lastInteraction !== null && lastInteraction != hitText) {
                lastInteraction.text.material = materials;
            }

            lastInteraction = hitText;
        }
    }
    else {
        if (lastInteraction !== null) {
            lastInteraction.text.material = materials;
        }

        lastInteraction = null;
    }
}

function handleCursorBounds(event) {
    var offset = isMode.get() ? settings.maxOffset : settings.minOffset;

    var min = new THREE.Vector3(-offset, 0.0, -offset);
    var max = new THREE.Vector3(offset, 0.0, offset);

    const mouseX = ((event.clientX / window.innerWidth) * 2 - 1) * cameraSizeX;
    const mouseY = ((event.clientY / window.innerHeight) * 2 - 1) * cameraSize;

    const pos = new THREE.Vector3(mouseX, mouseY, 0.0);

    if ((pos.x < min.x || pos.x > max.x || pos.y < min.z || pos.y > max.z) && isModeProp) {
        debugMaterial.color.set(1.0, 0.0, 0.0);

        Object.keys(homepageItems).forEach(id => {
            const result = pos.clone();
            result.y = -result.y;
            const offset = new THREE.Vector3();
            offset.copy(homepageItems[id].offsetDirection);
            offset.multiplyScalar(settings.followOffset);

            result.add(offset);

            homepageItems[id].targetPos = result;
            homepageItems[id].targetScale = false;
        });
    }
    else {
        debugMaterial.color.set(0.0, 1.0, 1.0);

        Object.keys(homepageItems).forEach(id => {
            let pos = new THREE.Vector3();

            pos.copy(homepageItems[id].offsetDirection);
            pos.multiplyScalar(isMode.get() ? settings.relaxedOffset : settings.originOffset);

            homepageItems[id].targetPos = pos;
            homepageItems[id].targetScale = true;
        });
    }
}

function handleMouseClick(event) {

    if (lastInteraction !== null) {
        handleInteractionClicked(lastInteraction.target.name);
    }
    else {
        handleInteractionClicked(null);
    }

    handleCursorBounds(event);
}

function handleInteractionClicked(target) {
    // target: String

    if (target != null) {
        isMode.set(true);
        createDebugHoverArea();

        let tab = homepageItems[target];

        tab.targetTab.show(tab.url);
    }
}

function handleKeyDown(event) {
    if (event.keyCode == 27) {
        isMode.set(false);
        createDebugHoverArea();
    }

    handleCursorBounds(event);
}

function handleOffsetChanged() {
    homepageItems['tab_art'].followOffset = new THREE.Vector3(-settings.followOffset, 0.0, 0.0);
    homepageItems['tab_blog'].followOffset = new THREE.Vector3(settings.followOffset, 0.0, 0.0);
    homepageItems['tab_music'].followOffset = new THREE.Vector3(0.0, 0.0, settings.followOffset);
    homepageItems['tab_reviews'].followOffset = new THREE.Vector3(0.0, 0.0, -settings.followOffset);
}

function handleTestChanged() {
    document.dispatchEvent(testEvent);
}

function handleHomepageButtonClicked(categoryObject) {
    categoryObject.targetTab.show(categoryObject.url);

    document.querySelectorAll('.homepage-button').forEach(b => {
        b.classList.add('is-active');
    });

    setTimeout(() => {
        document.querySelector('.homepage-root').style.pointerEvents = 'none';
    }, 150);
}

export { setRoot, update, setIsActive, setFont, createHomepageButtons, showHomepage, settings, testEvent, isMode }