let ChromaticShader = {
    uniforms: {
        tDiffuse: {
            value: null
        },
        shiftPower: {
            value: 2.81
        },
        shiftStep: {
            value: 0.508
        },
        shiftMult: {
            value: 0.045
        }
    },
    vertexShader:
    /* glsl */
    `
        varying vec2 vUv;

        void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }
        
    `,
    fragmentShader:
    /* glsl */
    `
        vec2 ShiftUV(vec2 uv, vec2 dir, float intensity) {
            return uv + vec2(dir.x * intensity, dir.y * intensity);
        }
        
        varying vec2 vUv;
        uniform sampler2D tDiffuse;
        uniform float shiftPower;
        uniform float shiftStep;
        uniform float shiftMult;

        void main() {
            vec2 center = vec2(0.5, 0.5);
            vec2 directedUv = center - vUv;
            float dirLength = length(directedUv);
            
            float uvPowR = pow(dirLength, shiftPower) * shiftMult;
            float uvPowG = pow(dirLength, shiftPower - shiftStep) * shiftMult;
            float uvPowB = pow(dirLength, shiftPower - shiftStep - shiftStep) * shiftMult;

            vec2 shiftDir = normalize(vec2(0.5, 0.5) - vUv);

            vec2 uvR = ShiftUV(vUv, shiftDir, uvPowR);
            vec2 uvG = ShiftUV(vUv, shiftDir, uvPowG);
            vec2 uvB = ShiftUV(vUv, shiftDir, uvPowB);

            float r = texture(tDiffuse, uvR).r;
            float g = texture(tDiffuse, uvG).g;
            float b = texture(tDiffuse, uvB).b;

            gl_FragColor = vec4(r, g, b, 1.0);
        }

    `
};

export { ChromaticShader }
