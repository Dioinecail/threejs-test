const shader = {
    uniforms: {
        tDiffuse: {
            value: null
        },
        iTime: {
            value: 0.0
        },
        aspect: {
            value: 0.5625
        }
    },
    vertexShader:
    /* glsl */
    `
        varying vec2 vUv;
        varying vec4 vPos;

        void main() {
            vUv = uv;
            vPos = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
            gl_Position = vPos;
        }
        
    `,
    fragmentShader:
    /* glsl */
    `
        varying vec2 vUv;
        varying vec4 vPos;

        uniform sampler2D tDiffuse;
        uniform float iTime;
        uniform float aspect;
        
        float rand(vec2 co)
        {
            return fract(sin(dot(co, vec2(12.9898, 78.233))) * 43758.5453);
        }

        void main()
        {
            vec2 vCoords = vPos.xy;
            vCoords /= vPos.w;
            vCoords.x *= aspect;
            vCoords = abs(vCoords); // [1 - 0 - 1]

            float dist = clamp(pow(length(vCoords), 5.0), 0.0, 1.0);

            vec2 flatUV = vUv;
            vec2 uv = vUv;

            flatUV.x = 0.0;
            
            flatUV.y *= 24.0;
            flatUV.y += 0.5;
            flatUV.y = round(flatUV.y);
            flatUV.y /= 24.0;
            
            float n = rand(flatUV);
            
            float yLines = (step(fract(uv.y * 12.0), 0.5) - 0.5) * 2.0;
            float rnd = yLines * 0.04 * n;

            float rR = fract(rnd * 43423.9102391) * 0.35;
            float rG = fract(rnd * 123411.19234913) * 0.25;
            float rB = fract(rnd * 91923.12391);

            uv.x += fract(iTime * rnd);
            
            vec4 c = texture(tDiffuse, uv);

            float alpha = c.r;

            c.rgb *= vec3(rR, rG, rB);
        
            if(alpha < 0.2)
                discard;

            gl_FragColor = vec4(c.rgb, 0.05 * dist);
            // gl_FragColor = vec4(dist, dist, dist, 1.0);
        }

    `
};

export { shader }