import * as THREE from 'three';
import * as PP from './PostProcess.js';
import * as Homepage from './Homepage.js';
import * as Music from './content/Music.js';
import * as Art from './content/Art.js';
import * as General from './content/General.js';
import * as Auth from './Auth.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { FontLoader } from 'three/addons/loaders/FontLoader.js';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import { CSS3DRenderer, CSS3DObject } from 'three/addons/renderers/CSS3DRenderer.js';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';
import { shader as ScrollingTextShader } from './shaders/ScrollingTextShader.js';
import { glitchPass } from './PostProcess.js';

const cursorElement = document.querySelector('#cursor-circle');
const clock = new THREE.Clock();
const inputTimer = new THREE.Clock();
const messageTimer = new THREE.Clock(false);
const cameraSize = 2;
const cameraSizeMultMobile = 1;
const cursorPosition = {
    x: 0,
    y: 0
}
const cursorSpeed = 0.05;

var selectionCursor = {
    root: null,
    animation: null,
    animationMixer: null
}

let aspectRatio = isMobileBrowser()
    ? window.innerHeight / window.innerWidth
    : window.innerWidth / window.innerHeight;
let totalCameraSize = isMobileBrowser() ? cameraSize * cameraSizeMultMobile : cameraSize;
let cameraSizeX = isMobileBrowser() ? totalCameraSize : aspectRatio * totalCameraSize;
let cameraSizeY = isMobileBrowser() ? aspectRatio * totalCameraSize : totalCameraSize;
let rootAnimation;

let logonObjects = [
    'circle1',
    'circle2',
    'circle3',
    'circle3001',
    'circle3002',
    'circle3003',
    'circle3004',
    'circle3005',
    'circle3006',
    'circle3007',
    'circle3008',
    'circle3009',
    'circle3010',
    'enter_id',
    'enter_id_jap',
    'id_line1',
    'id_line2',
    'input_cube',
    'text_logon'
]

let logonScene;
let inputActive;

const loader = new GLTFLoader();
const fontLoader = new FontLoader();
const scene = new THREE.Scene();
const camera = new THREE.OrthographicCamera(-cameraSizeX, cameraSizeX, cameraSizeY, -cameraSizeY, 0.1, 1000);
// const camera = new THREE.PerspectiveCamera(90.0, aspectRatio, 0.1, 1000.0);

let inputAnchor;
let inputCube;
let messages = [];

camera.translateZ(4);

const renderer = new THREE.WebGLRenderer();
const cssRenderer = new CSS3DRenderer({ element: document.querySelector('#css-renderer')});
cssRenderer.domElement.style.position = 'absolute';
cssRenderer.domElement.style.top = 0;
cssRenderer.domElement.id = 'css-renderer';
cssRenderer.setSize(window.innerWidth, window.innerHeight);
// const controls = new OrbitControls(camera, cssRenderer.domElement);
// controls.rotateSpeed = 5.0;

let inputTextGeometry;
let inputText = '';
let inputTextMesh;

let materials = [
    new THREE.MeshBasicMaterial({ color: 0x009C9B }), // front
    new THREE.MeshBasicMaterial({ color: 0x009C9B }) // side
];

const scrollingTextMaterial = new THREE.ShaderMaterial(ScrollingTextShader);
scrollingTextMaterial.transparent = true;
scrollingTextMaterial.uniforms.aspect.value = aspectRatio;
let time = 0.0;

renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setClearColor(0x020204);
renderer.domElement.id = 'threejs-renderer';

document.body.appendChild(renderer.domElement);
document.addEventListener('keydown', onDocumentKeyDown);
document.addEventListener('keypress', onDocumentKeyPress);
document.addEventListener('mousemove', handleMouseMoved);

window.addEventListener('resize', onWindowResize);

PP.initPostProcess(renderer, camera, scene);

General.init(() => {
    console.log("General is ready");

    Homepage.setFont(General.settings.font);

    let previouslyInputtedName = localStorage.getItem('input-name');

    if (previouslyInputtedName !== null) {
        inputText = previouslyInputtedName;
    }

    refreshText();

    loader.load('content/staticOS.glb', function (gltf) {
        inputTimer.start();
        inputActive = true;

        let logon = gltf.scene.getObjectByName('logon');

        inputCube = logon.getObjectByName('input_cube');
        inputAnchor = logon.getObjectByName('input_anchor');

        let scrollingTextLinesRoot = gltf.scene.getObjectByName('scrolling_text');

        scrollingTextLinesRoot.children.forEach(elem => {
            scrollingTextMaterial.uniforms.tDiffuse.value = elem.material.emissiveMap;
            elem.material = scrollingTextMaterial;
        });

        const messageWarning = logon.getObjectByName('id_warning');
        const messageSuccess = logon.getObjectByName('id_success');

        let homeRoot = gltf.scene.getObjectByName('homepage');

        Homepage.setRoot(homeRoot);

        messageWarning.visible = false;
        messageSuccess.visible = false;

        messages.push(messageWarning, messageSuccess);

        logonScene = gltf.scene;

        inputCube.position.x = inputAnchor.position.x + (inputText.length * 0.12);
        inputCube.position.y = inputAnchor.position.y - 0.04;
        inputCube.position.z = inputAnchor.position.z;

        inputTextMesh.position.x = inputAnchor.position.x;
        inputTextMesh.position.y = inputAnchor.position.y;
        inputTextMesh.position.z = inputAnchor.position.z;

        rootAnimation = new THREE.AnimationMixer(logonScene);

        gltf.animations.forEach(clip => {
            let anim = rootAnimation.clipAction(clip);
            anim.play();
        });

        rootAnimation.animations = gltf.animations;

        scene.add(logonScene);

        // hideLogon();
        // messageTimer.start();
    }, undefined, function (error) {
        console.error(error);
    });
});

function refreshText() {
    if (inputTextMesh !== undefined) {
        scene.remove(inputTextMesh);
    }

    inputTextGeometry = new TextGeometry(inputText, {
        font: General.settings.font,
        size: 0.14,
        height: 0.1,
        curveSegments: 8
    });

    inputTextGeometry.computeBoundingBox();

    inputTextMesh = new THREE.Mesh(inputTextGeometry, materials);

    if (inputAnchor !== undefined) {
        inputTextMesh.position.x = inputAnchor.position.x;
        inputTextMesh.position.y = inputAnchor.position.y;
        inputTextMesh.position.z = inputAnchor.position.z;
    }

    if (inputCube !== undefined) {
        let ap = inputAnchor.position;
        let letterCount = inputText.length;

        inputCube.position.x = ap.x + (letterCount * 0.12);
        inputCube.position.y = ap.y - 0.04;
        inputCube.position.z = ap.z;
    }

    localStorage.setItem('input-name', inputText);

    scene.add(inputTextMesh);
}

function onDocumentKeyDown(event) {
    const keyCode = event.keyCode;

    // backspace
    if (keyCode == 8) {

        event.preventDefault();

        inputText = inputText.substring(0, inputText.length - 1);
        refreshText();

        return false;
    }

    // enter
    if (keyCode == 13) {
        if (messageTimer.running) {
            messageTimer.stop();
        }

        if (inputText) {
            messages[1].visible = true;

            hideLogon();
            Auth.tryAddUser(inputText);
        }
        else {
            messages[0].visible = true;

            setTimeout(() => {
                messages[0].visible = false;
            }, 1000);
        }

        if (isMobileBrowser()) {
            document.querySelector('#logon-input-mobile').style.display = 'none';
        }
    }
}

function onDocumentKeyPress(event) {
    const keyCode = event.which;

    // backspace || enter
    if (keyCode == 8 || keyCode == 13) {
        event.preventDefault();
    }
    else {
        const ch = String.fromCharCode(keyCode);
        inputText += ch;

        refreshText();
    }
}

function onWindowResize() {
    // aspectRatio = window.innerWidth / window.innerHeight;
    // cameraSizeX = aspectRatio * cameraSize;

    // camera.aspect = window.innerWidth / window.innerHeight;
    // camera.left = -cameraSizeX;
    // camera.right = cameraSizeX;
    // camera.updateProjectionMatrix();

    // renderer.setPixelRatio(window.devicePixelRatio);
    // renderer.setSize(window.innerWidth, window.innerHeight);
    // cssRenderer.setSize(window.innerWidth, window.innerHeight);

    // scrollingTextMaterial.uniforms.aspect.value = aspectRatio;
}

function hideLogon() {
    document.removeEventListener('keydown', onDocumentKeyDown);
    document.removeEventListener('keypress', onDocumentKeyPress);

    loader.load('content/cursor_circle.glb', function (gltf) {
        setTimeout(() => {
            glitchPass.isAlwaysOn = true;
            
            setTimeout(() => {
                glitchPass.isAlwaysOn = false;
            }, 35);
        }, 500);

        setTimeout(() => {
            glitchPass.isAlwaysOn = true;

            setTimeout(() => {
                glitchPass.isAlwaysOn = false;
            }, 35);
        }, 650);


        setTimeout(() => {
            glitchPass.isAlwaysOn = true;

            let cursorRoot = gltf.scene.getObjectByName('circle_root');
    
            selectionCursor.root = cursorRoot;
            selectionCursor.animationMixer = new THREE.AnimationMixer(gltf.scene);
    
            gltf.animations.forEach(clip => {
                let anim = selectionCursor.animationMixer.clipAction(clip);
                anim.play();
            });
    
            selectionCursor.animations = gltf.animations;
    
            scene.add(cursorRoot);
    
            Homepage.createHomepageButtons();
    
            let logonItems = logonScene.children[0].children;

            logonItems.forEach(elem => {
                if (logonObjects.includes(elem.name)) {
                    elem.visible = false;
                }
            });
    
            inputActive = false;
            inputTimer.stop();
            inputCube.visible = false;
            inputTextMesh.visible = false;
    
            Homepage.setIsActive(true);
            messages[1].visible = false;

            setTimeout(() => {
                glitchPass.isAlwaysOn = false;
            }, 150);
        }, 1200);

        setTimeout(() => {
            glitchPass.isAlwaysOn = true;

            setTimeout(() => {
                glitchPass.isAlwaysOn = false;
            }, 35);
        }, 1550);

    }, undefined, (error) => {
        console.error(error);
    });

}

function update() {
    requestAnimationFrame(update);

    let delta = clock.getDelta();
    let elapsedTime = inputTimer.getElapsedTime();

    time += delta;

    scrollingTextMaterial.uniforms.iTime.value = time;

    if (elapsedTime > 0.8 && inputCube !== undefined && inputActive) {
        inputCube.visible = !inputCube.visible;
        inputTimer.start();
    }

    if (rootAnimation !== undefined) {
        rootAnimation.update(delta);
    }

    if (selectionCursor.animationMixer !== null) {
        selectionCursor.animationMixer.update(delta);
    }

    Homepage.update();
    Music.update(delta);

    // controls.update();
    PP.render();
    cssRenderer.render(scene, camera);

    updateCursorPosition();
}

function isMobileBrowser() {
    if (window.isMobileBrowser !== undefined)
        return window.isMobileBrowser;

    // let gl = canvas.getContext('webgl2');
    // let isMobileGraphics = (gl !== undefined && gl.getExtension('WEBGL_compressed_texture_astc') !== undefined);

    let isMobileNavigator = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

    let isAppleProduct = navigator.userAgent.match(/Mac/) !== null;
    let isIphone = navigator.userAgent.match(/iPhone/) !== null;

    let isIpad = ((isAppleProduct && !isIphone) && navigator.maxTouchPoints !== null && navigator.maxTouchPoints > 2);

    let isResult = (isMobileNavigator || isIpad);

    window.isMobileBrowser = isResult;

    return isResult;
}

function updateCursorPosition() {
    // cursorElement.style.left = THREE.MathUtils.lerp(cursorElement.style.left.replace('px', ''), cursorPosition.x, cursorSpeed);
    // cursorElement.style.top = THREE.MathUtils.lerp(cursorElement.style.top.replace('px', ''), cursorPosition.y, cursorSpeed);
}

function handleMouseMoved(evt) {
    cursorPosition.x = evt.x;
    cursorPosition.y = evt.y;
}

update();

export { camera, cameraSize, cameraSizeX, scene }