import * as THREE from 'three';
import * as General from './General.js';

var linksUrl;
var rootUrl;
var onBackEvents = [];



function addEventListener(event) {
    onBackEvents.push(event);
}

function show(url) {
    console.log('This is Links tab');

    rootUrl = url;
    linksUrl = `${url}links.json`;

    fetchData(linksUrl, handleLinksFetched);

    General.contentRootDiv.style.display = 'flex';
    General.buttonBack.style.display = 'block';
    General.buttonBack.addEventListener('click', handleBackClicked);
    document.addEventListener('wheel', handleScrollWheel);
    document.addEventListener("touchstart", handleTouchStart);
    document.addEventListener("touchmove", handleTouchMove);
}

function hide() {
    General.buttonBack.style.display = 'none';
    General.contentRootDiv.style.display = 'none';
    General.foldersRootDiv.style.displa = 'none';

    onBackEvents.forEach((event) => {
        event();
    });

    General.buttonBack.removeEventListener('click', handleBackClicked);
    document.removeEventListener('wheel', handleScrollWheel);
    document.removeEventListener("touchstart", handleTouchStart);
    document.removeEventListener("touchmove", handleTouchMove);
}

function fetchData(url, onFetched) {
    fetch(url, { cache: "no-cache" })
        .then(res => {
            return res.json();
        })
        .then(data => {
            onFetched(data);
        });
}

function handleLinksFetched(data) {
    data.content.forEach(link => {
        let div = document.createElement('div');

        if (link.includes('[space]')) {
            div.className = 'link-space';
        }
        else {
            div.className = 'link-container';
            div.innerHTML = link;
        }

        General.contentRootDiv.appendChild(div);
    });
}

function handleScrollWheel(event) {
    General.contentGroup.position.y += event.deltaY * General.settings.scrollSpeed;
}

function handleTouchStart(event) {
    General.touchStartCoord.y = event.touches[0].screenY;
    General.touchStartCoord.start = General.contentGroup.position.y;
}

function handleTouchMove(event) {
    let delta = event.touches[0].screenY - General.touchStartCoord.y;

    General.contentGroup.position.y = General.touchStartCoord.start - delta * General.settings.scrollSpeedMobile;
}

function handleBackClicked() {
    hide();
}

export { show, hide, addEventListener }