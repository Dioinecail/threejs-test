import * as THREE from 'three';
import * as General from './General.js';
import { scene } from '../main.js';

const artContentGroup = new THREE.Group();
const testUrl = 'https://raw.githubusercontent.com/Dioinecail/staticOS-content/master/art/folders.json';
const testUrlLocal = '../content/json-file.json';

function show() {
    console.log('This is Art tab');

    scene.add(artContentGroup);

    // fetch('https://raw.githubusercontent.com/Dioinecail/staticOS-content/master/art/001.jpg')
    //     .then(res => {
    //         console.log(res);
    //     });

    fetchData(testUrlLocal, parseContent);

    document.addEventListener('wheel', handleScrollWheel);
    document.addEventListener("touchstart", handleTouchStart);
    document.addEventListener("touchmove", handleTouchMove);
}

function hide() {
    document.removeEventListener('wheel');
    document.removeEventListener("touchstart", handleTouchStart);
    document.removeEventListener("touchmove", handleTouchMove);
}

function fetchData(url, onFetched) {
    fetch(url)
        .then(res => {
            return res.json();
        })
        .then(data => {
            onFetched(data);
        });
}

function parseContent(data) {
    data.content.forEach((elem, index, arr) => {
        console.log(elem);
        switch (elem.type) {
            case 'text': {
                let width = elem.width;
                const str = elem.contents;
                let linedText = [];
                let numLines = 0;

                for (let i = 0; i < str.length; i += width) {
                    let start = i;
                    let end = i + width;
                    linedText.push(`${str.slice(start, end)}\n`);
                    numLines++;
                }

                var textObject = General.createText(linedText.join(''), numLines);

                artContentGroup.add(textObject);
                textObject.position.set(...elem.position);
                break;
            }
            case 'pic': {
                let width = elem.width;
                let height = elem.height;
                let url = elem.contents;

                var artCard = General.createPicture();

                General.textureLoader.load(url, (tex) => {
                    const material = new THREE.MeshBasicMaterial({
                        map: tex
                    });

                    artCard.material = material;
                });

                artContentGroup.add(artCard);

                artCard.scale.set(width, height, 0.0);
                artCard.position.set(...elem.position);

                break;
            }
            default:
                break;
        }
    });
}

function handleScrollWheel(event) {
    artContentGroup.position.y += event.deltaY * General.settings.scrollSpeed;
}

function handleTouchStart(event) {
    General.touchStartCoord.y = event.touches[0].screenY;
    General.touchStartCoord.start = General.contentGroup.position.y;
}

function handleTouchMove(event) {
    let delta = event.touches[0].screenY - General.touchStartCoord.y;

    General.contentGroup.position.y = General.touchStartCoord.start - delta * General.settings.scrollSpeedMobile;
}

export { show, hide }