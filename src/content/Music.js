import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { CSS3DRenderer, CSS3DObject } from 'three/addons/renderers/CSS3DRenderer.js';
import { scene } from '../main.js';
import { settings } from '../Homepage.js';
import * as General from './General.js';

var foldersJsonUrl;
var rootUrl;
var isViewContent = false;
var onBackEvents = [];

var musicPlayer = {
    model: null,
    animation: null,
    nameplateAnchor: null,
    nameplate: null,
    state: 'active',
    lerpSpeed: 2.0
};

var musicIframe = null;
const contentFolders = [];

var currentFolder = null;
var currentMusic = null;
var state = {
    chop: false
}

const musicPlayerPosition = {
    active: new THREE.Vector3(0.72, 0.732, 0.364),
    inactive: new THREE.Vector3(1.14, -1.572, 0.532)
};
const musicPlayerRotation = {
    active: new THREE.Vector3(0.854513201776424, 0.295309709437441, 0.0),
    inactive: new THREE.Vector3(0.0, 0.0, 0.0)
}
const musicPlayerScale = {
    active: new THREE.Vector3(2.0, 2.0, 2.0),
    inactive: new THREE.Vector3(1.0, 1.0, 1.0)
}



function addEventListener(event) {
    onBackEvents.push(event);
}

function show(url) {
    console.log('This is Music tab');

    rootUrl = url;
    foldersJsonUrl = `${url}folders.json`;

    General.buttonBack.style.display = 'block';
    General.foldersRootDiv.style.display = 'grid';
    General.contentGroup.position.set(0.0, 0.0, 0.0);
    General.contentRootDiv.style['align-items'] = 'baseline';

    General.fetchData(foldersJsonUrl, parseFolders);

    if (musicPlayer.model === null) {
        loadPlayer();
    }

    musicPlayer.state = 'active';

    General.buttonBack.addEventListener('click', handleBackClicked);
    document.addEventListener('wheel', handleScrollWheel);
    document.addEventListener("touchstart", handleTouchStart);
    document.addEventListener("touchmove", handleTouchMove);
}

function hide() {
    General.buttonBack.style.display = 'none';
    General.contentRootDiv.style.display = 'none';
    General.foldersRootDiv.style.display = 'none';
    General.contentRootDiv.style['align-items'] = 'center';

    onBackEvents.forEach((event) => {
        event();
    });

    musicPlayer.state = 'inactive';

    General.buttonBack.removeEventListener('click', handleBackClicked);
    document.removeEventListener('wheel', handleScrollWheel);
    document.removeEventListener("touchstart", handleTouchStart);
    document.removeEventListener("touchmove", handleTouchMove);
}

function parseFolders(data) {
    contentFolders.length = 0;

    General.clearContent();
    General.contentGroup.position.set(0.0, 0.0, 0.0);
    General.contentRootDiv.style.display = 'none';
    General.foldersRootDiv.style.display = 'grid';

    isViewContent = false;

    Object.keys(data.contents).forEach(key => {
        var folder = data.contents[key];
        folder.id = key;

        contentFolders.push(folder);
    });

    contentFolders.sort((a, b) => {
        return a.index - b.index;
    });

    contentFolders.forEach((elem, index, arr) => {
        createContentFolder(elem);
    });
}

function createContentFolder(folder) {
    let bannerUrl = rootUrl + folder.banner;

    let folderContainer = document.createElement('div');
    folderContainer.className = 'blog-folder-container';

    let folderObject = document.createElement('div');
    folderObject.className = 'blog-folder';

    let folderPicGradient = document.createElement('div');
    folderPicGradient.className = 'blog-folder-banner-gradient';

    let picObject = document.createElement('img');
    picObject.className = 'blog-folder-banner';
    picObject.src = bannerUrl;

    let header = document.createElement('div');
    header.className = 'blog-folder-header';
    header.textContent = folder.title;

    folderContainer.appendChild(folderObject);
    folderObject.appendChild(header);
    folderObject.appendChild(folderPicGradient);
    folderObject.appendChild(picObject);

    folderContainer.addEventListener('click', () => {

        currentFolder = folder.id;

        var url = `${rootUrl}${folder.id}/content.json`;

        General.fetchData(url, parseContent);
    });

    General.foldersRootDiv.appendChild(folderContainer);
}

function loadPlayer() {
    if (musicPlayer.model === null) {

        General.gltfLoader.load('content/vinyl_player.glb', (gltf) => {
            musicPlayer.model = new THREE.Group();
            musicPlayer.model.position.copy(musicPlayerPosition.active);
            musicPlayer.model.add(gltf.scene);

            musicPlayer.animation = new THREE.AnimationMixer(gltf.scene);

            gltf.animations.forEach(clip => {
                let anim = musicPlayer.animation.clipAction(clip);
                anim.play();
            });

            musicPlayer.animation.animations = gltf.animations;
            musicPlayer.nameplateAnchor = musicPlayer.model.getObjectByName('plate_music');

            scene.add(musicPlayer.model);
        });
    }
}

function playMusic(url) {
    if (currentMusic !== null && currentMusic == url) {
        return;
    }

    if (musicIframe !== null) {
        musicPlayer.nameplateAnchor.remove(musicPlayer.nameplate);
    }

    var div = document.createElement('div');
    musicIframe = document.createElement('iframe');

    musicIframe.style.width = '300px';
    musicIframe.style.height = '42px';
    musicIframe.style.borderWidth = '0px';
    musicIframe.style.scale = '0.03298';
    musicIframe.style.zIndex = '-10';
    musicIframe.src = url;

    div.appendChild(musicIframe);

    // var chop = document.getElementById('bandcamp-player').contentWindow;
    musicPlayer.nameplate = new CSS3DObject(div);

    musicPlayer.nameplateAnchor.add(musicPlayer.nameplate);
    musicPlayer.nameplate.rotation.set(0.0, 0.0, 0.0);
}

function parseContent(data) {
    isViewContent = true;

    General.clearFolders();

    General.contentGroup.position.set(0.0, 0.0, 0.0);
    General.contentRootDiv.style.display = 'flex';
    General.foldersRootDiv.style.display = 'none';

    data.content.forEach((elem) => {
        let textObject = document.createElement('div');
        let url = elem.url;
        let title = elem.title;

        textObject.textContent = title;

        textObject.className = 'music-link-container';
        textObject.addEventListener('click', () => {
            playMusic(url);
        });

        General.contentRootDiv.appendChild(textObject);
    });
}

function update(delta) {
    if (musicPlayer.animation !== null) {
        musicPlayer.animation.update(delta);
    }

    if (musicPlayer.model !== null) {
        let targetPos = musicPlayerPosition[musicPlayer.state];
        let targetRot = musicPlayerRotation[musicPlayer.state];
        let targetScale = musicPlayerScale[musicPlayer.state];

        let originPos = musicPlayer.model.position;
        let originRot = musicPlayer.model.rotation;
        let originScale = musicPlayer.model.scale;

        let xPos = THREE.MathUtils.lerp(originPos.x, targetPos.x, delta * musicPlayer.lerpSpeed);
        let yPos = THREE.MathUtils.lerp(originPos.y, targetPos.y, delta * musicPlayer.lerpSpeed);
        let zPos = THREE.MathUtils.lerp(originPos.z, targetPos.z, delta * musicPlayer.lerpSpeed);

        let xRot = THREE.MathUtils.lerp(originRot.x, targetRot.x, delta * musicPlayer.lerpSpeed);
        let yRot = THREE.MathUtils.lerp(originRot.y, targetRot.y, delta * musicPlayer.lerpSpeed);
        let zRot = THREE.MathUtils.lerp(originRot.z, targetRot.z, delta * musicPlayer.lerpSpeed);

        let xScl = THREE.MathUtils.lerp(originScale.x, targetScale.x, delta * musicPlayer.lerpSpeed);
        let yScl = THREE.MathUtils.lerp(originScale.x, targetScale.x, delta * musicPlayer.lerpSpeed);
        let zScl = THREE.MathUtils.lerp(originScale.x, targetScale.x, delta * musicPlayer.lerpSpeed);

        musicPlayer.model.position.set(xPos, yPos, zPos);
        musicPlayer.model.rotation.set(xRot, yRot, zRot);
        musicPlayer.model.scale.set(xScl, yScl, zScl);
    }
}

function handleMusicPlayerStateChanged() {
    musicPlayer.state = state.chop ? 'active' : 'inactive';
}

function handleScrollWheel(event) {
    General.contentGroup.position.y += event.deltaY * General.settings.scrollSpeed;
}

function handleTouchStart(event) {
    General.touchStartCoord.y = event.touches[0].screenY;
    General.touchStartCoord.start = General.contentGroup.position.y;
}

function handleTouchMove(event) {
    let delta = event.touches[0].screenY - General.touchStartCoord.y;

    General.contentGroup.position.y = General.touchStartCoord.start - delta * General.settings.scrollSpeedMobile;
}

function handleBackClicked() {
    if (isViewContent) {
        General.fetchData(foldersJsonUrl, parseFolders);
    }
    else {
        hide();
    }
}

export { show, hide, update, addEventListener }