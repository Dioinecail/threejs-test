import * as THREE from 'three';
import * as General from './General.js';
import * as Likes from './Likes.js';
import * as Auth from '../Auth.js';

var foldersJsonUrl;
var rootUrl;

var onBackEvents = [];

//#region for likes
var likesIcon;
var likesNum;
var currentLikesCount = 0;
var contentType = null;
//#endregion

// will be like 'parentFolderId/subFolderId/blogId'
var folderAddress = '';
var folderDatas = [];



function addEventListener(event) {
    onBackEvents.push(event);
}

function show(url) {
    rootUrl = url;
    foldersJsonUrl = `${url}content.json`;

    General.buttonBack.style.display = 'block';
    General.foldersRootDiv.style.display = 'grid';
    General.contentGroup.position.set(0.0, 0.0, 0.0);

    General.fetchData(foldersJsonUrl, parseFolders);

    General.buttonBack.addEventListener('click', handleBackClicked);
    document.addEventListener('wheel', handleScrollWheel);
    document.addEventListener("touchstart", handleTouchStart);
    document.addEventListener("touchmove", handleTouchMove);
}

function hide() {
    General.buttonBack.style.display = 'none';
    General.contentRootDiv.style.display = 'none';
    General.foldersRootDiv.style.displa = 'none';

    General.buttonBack.removeEventListener('click', handleBackClicked);
    document.removeEventListener('wheel', handleScrollWheel);
    document.removeEventListener("touchstart", handleTouchStart);
    document.removeEventListener("touchmove", handleTouchMove);

    onBackEvents.forEach((event) => {
        event();
    });
}

function hideSubfolder() {
    if (subfolderContent != null) {
        parseContent(subfolderContent);

        subfolderContent = null;
        isSubfolderContent = false;
    }
}

// general method for viewing content or folders
function parseFolders(data) {
    folderDatas.push(data);
    console.log(folderDatas);

    if (data.content) {
        parseContent(data.content);
    }

    if (data.contents) {
        parseSubfolder(data.contents);
    }
}

function parseFoldersBack(data) {
    console.log(folderDatas);
    
    if (data.content) {
        parseContent(data.content);
    }

    if (data.contents) {
        parseSubfolder(data.contents);
    }
}

function parseSubfolder(data) {
    General.clearContent();
    General.clearFolders();
    General.contentGroup.position.set(0.0, 0.0, 0.0);
    General.contentRootDiv.style.display = 'none';
    General.foldersRootDiv.style.display = 'grid';

    let contentFolders = [];

    Object.keys(data).forEach(key => {
        var folder = data[key];

        folder.id = key;

        contentFolders.push(folder);
    });

    contentFolders.sort((a, b) => {
        return a.index - b.index;
    });

    contentFolders.forEach((elem) => {
        createContentFolder(elem);
    });
}

function parseContent(data) {
    // Likes.requestLikes(currentBlogId, contentType).then((likesCount) => {
    //     currentLikesCount = likesCount;

    //     if (likesNum !== null) {
    //         likesNum.textContent = `${currentLikesCount}`;
    //     }
    // });
    General.clearFolders();
    General.clearContent();

    General.contentGroup.position.set(0.0, 0.0, 0.0);
    General.contentRootDiv.style.display = 'flex';
    General.foldersRootDiv.style.display = 'none';

    data.forEach((elem) => {
        switch (elem.type) {
            case 'text': {
                var textObject = document.createElement('div');

                textObject.innerHTML = elem.contents;
                textObject.className = 'blog-text';

                General.contentRootDiv.appendChild(textObject);
                break;
            }
            case 'pic': {
                let url = `${rootUrl}${folderAddress}/${elem.contents}`;

                var picObject = document.createElement('img');

                picObject.src = url;
                picObject.className = 'blog-pic';

                General.contentRootDiv.appendChild(picObject);
                break;
            }
            default:
                break;
        }
    });

    // createLikesPanel();
}

function createContentFolder(folder) {
    if ('isPublished' in folder && folder.isPublished)
        return;

    let bannerUrl = rootUrl + folder.banner;

    let folderContainer = document.createElement('div');
    folderContainer.className = 'blog-folder-container';

    let folderObject = document.createElement('div');
    folderObject.className = 'blog-folder';

    let folderPicGradient = document.createElement('div');
    folderPicGradient.className = 'blog-folder-banner-gradient';

    let picObject = document.createElement('img');
    picObject.className = 'blog-folder-banner';
    picObject.src = bannerUrl;

    let header = document.createElement('div');
    header.className = 'blog-folder-header';
    header.textContent = folder.title;

    folderContainer.appendChild(folderObject);
    folderObject.appendChild(header);
    folderObject.appendChild(folderPicGradient);
    folderObject.appendChild(picObject);

    folderContainer.addEventListener('click', (evt) => {

        // example (no subfolders): 0001
        // example (1 subfolder):   0001/0001
        folderAddress = folder.id;

        // example (root):      root.url/{content-type}/content.json
        // example (subfolder): root.url/{content-type}/0001/content.json
        var url = `${rootUrl}${folder.id}/content.json`;

        General.fetchData(url, parseFolders);
    });

    General.foldersRootDiv.appendChild(folderContainer);
}

function createLikesPanel() {
    let likesPanel = document.createElement('div');
    likesNum = document.createElement('div');
    likesIcon = document.createElement('div');

    likesPanel.className = 'likes-container';
    likesNum.className = 'likes-number';
    likesIcon.className = 'likes-icon like-icon-nolike';
    likesNum.textContent = `${currentLikesCount}`;

    let isLiked = Likes.tryGetLike(currentBlogId, contentType);

    if (isLiked) {
        likesIcon.classList.remove('like-icon-nolike');
        likesIcon.classList.add('like-icon-liked');
    }

    likesIcon.addEventListener('click', handleBlogLikeClicked);

    likesPanel.appendChild(likesNum);
    likesPanel.appendChild(likesIcon);
    General.contentRootDiv.appendChild(likesPanel);
}

function handleBlogLikeClicked() {
    let isLiked = Likes.tryGetLike(currentBlogId, contentType);

    if (isLiked) {
        console.log(`You no longer like this blog! Blog id: '${currentBlogId}'`);
        Likes.removeLike(currentBlogId, contentType, Auth.userId);
        likesIcon.classList.remove('like-icon-liked');
        likesIcon.classList.add('like-icon-nolike');
        likesNum.textContent = `${currentLikesCount}`;
    }
    else {
        console.log(`You liked this blog! Blog id: '${currentBlogId}'`);
        Likes.addLike(currentBlogId, contentType, Auth.userId);
        likesIcon.classList.remove('like-icon-nolike');
        likesIcon.classList.add('like-icon-liked');
        likesNum.textContent = `${currentLikesCount + 1}`;
    }
}

function handleScrollWheel(event) {
    General.contentGroup.position.y += event.deltaY * General.settings.scrollSpeed;
}

function handleTouchStart(event) {
    General.touchStartCoord.y = event.touches[0].screenY;
    General.touchStartCoord.start = General.contentGroup.position.y;
}

function handleTouchMove(event) {
    let delta = event.touches[0].screenY - General.touchStartCoord.y;

    General.contentGroup.position.y = General.touchStartCoord.start - delta * General.settings.scrollSpeedMobile;
}

function handleBackClicked() {
    if (folderAddress.length > 0) {
        folderDatas.pop();

        if (folderDatas.length > 0) {
            let parentFolderData = folderDatas[folderDatas.length - 1];
    
            parseFoldersBack(parentFolderData);
        }
        else {
            hide();
        }
    }
    else {
        hide();
    }
}

export { show, hide, addEventListener }