import * as THREE from 'three';
import * as Blog from './Blog.js';
import * as Music from './Music.js';
import * as Links from './Links.js';
import * as Homepage from '../Homepage.js';
import * as Portfolio from './Portfolio.js';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';
import { FontLoader } from 'three/addons/loaders/FontLoader.js';
import { TextGeometry } from 'three/addons/geometries/TextGeometry.js';
import { CSS3DRenderer, CSS3DObject } from 'three/addons/renderers/CSS3DRenderer.js';
import { scene } from '../main.js';

const fontUrl = 'content/fonts/Space_Mono_Regular.json';
const textMaterials = [
    new THREE.MeshBasicMaterial({ color: 0x009C9B }), // front
    new THREE.MeshBasicMaterial({ color: 0x009C9B }) // side
]

const settings = {
    font: null,
    fontSize: 0.03,
    headerFontSize: 0.0065,
    fontHeight: 0.01,
    curveSegments: 6,
    scrollSpeed: 0.002,
    scrollSpeedMobile: 0.01,
    scalePerLine: 1.2,
    foldersOffset: 0.7,
    foldersRootOffset: new THREE.Vector3(-1.35, 1.35, 0.0)
};

const textureLoader = new THREE.TextureLoader();
const contentGroup = new THREE.Group();
const gltfLoader = new GLTFLoader();
const fontLoader = new FontLoader();

var rootDiv;
var contentRootDiv;
var foldersRootDiv;
var buttonBack;
var touchStartCoord = {
    y: 0,
    start: 0
};



function init(onReady) {

    rootDiv = document.createElement('div');
    rootDiv.className = 'blog-root';

    contentRootDiv = document.createElement('div');
    contentRootDiv.className = 'blog-content-root';
    contentRootDiv.style.display = 'none';

    foldersRootDiv = document.createElement('div');
    foldersRootDiv.className = 'blog-folders-root';

    buttonBack = document.querySelector('.btn-back')

    rootDiv.appendChild(contentRootDiv);
    rootDiv.appendChild(foldersRootDiv);

    let rootObj = new CSS3DObject(rootDiv);

    rootObj.scale.set(0.007, 0.007, 0.007);
    rootObj.position.set(0.0, 1.35, 2.0);

    contentGroup.add(rootObj);
    scene.add(contentGroup);

    fontLoader.load(fontUrl, (loadedFont) => {
        settings.font = loadedFont;

        onReady();
    });

    Blog.addEventListener(handleBackClicked);
    Music.addEventListener(handleBackClicked);
    Links.addEventListener(handleBackClicked);
    Portfolio.addEventListener(handleBackClicked);
}

function createText(text, numlines, position, rotation) {
    var textDiv = document.createElement('div');
    textDiv.className = 'blog-text';
    textDiv.textContent = text;
    textDiv.style.backgroundColor = '#202020';

    var textObject = new CSS3DObject(textDiv);

    if (position !== undefined)
        textObject.position.copy(position);

    if (rotation !== undefined) {
        textObject.rotation.copy(rotation);
    }

    return textObject;
}

function createContentFolder() {
    return settings.contentFolderCard.clone();
}

function clearContent() {
    contentRootDiv.replaceChildren();
}

function clearFolders() {
    foldersRootDiv.replaceChildren();
}

function handleBackClicked() {
    Homepage.isMode.set(false);
    Homepage.showHomepage();
    clearContent();
    clearFolders();
}

function fetchData(url, onFetched) {
    fetch(url, { cache: "no-cache" })
        .then(res => {
            return res.json();
        })
        .then(data => {
            onFetched(data);
        });
}

function createElementFromHtml(html, searchId, tag, id, targetContainer) {
    let template = document.createElement('div');

    var resultId = `${tag}-${id}`;

    targetContainer.appendChild(template);
    template.outerHTML = html.replace(`{${searchId}}`, resultId);

    var div = targetContainer.querySelector(`#${resultId}`);

    return div;
}

export {
    settings,
    init,
    createText,
    createContentFolder,
    clearContent,
    clearFolders,
    fetchData,
    createElementFromHtml,
    textureLoader,
    gltfLoader,
    fontLoader,
    textMaterials,
    rootDiv,
    contentRootDiv,
    foldersRootDiv,
    buttonBack,
    contentGroup,
    touchStartCoord
}