const addLikeUrl = 'https://us-east-1.aws.data.mongodb-api.com/app/application-0-qvaub/endpoint/likes/addLike';
const removeLikeUrl = 'https://us-east-1.aws.data.mongodb-api.com/app/application-0-qvaub/endpoint/likes/removeLike';
const getTotalLikesUrl = 'https://us-east-1.aws.data.mongodb-api.com/app/application-0-qvaub/endpoint/likes/getTotalLikes';

const likeKeyBase = 'staticos-like';



function addLike(contentId, contentType, userId) {
    let likeKey = `${likeKeyBase}-${contentType}-${contentId}`;

    localStorage.setItem(likeKey, '1');

    const request = {
        cache: "no-cache",
        method: "POST",
        body: JSON.stringify({
            contentId: contentId,
            contentType: contentType,
            userId: userId
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    };

    fetch(addLikeUrl, request)
        .then((res) => {
            return res.json();
        })
        .then(data => {
            console.log({data});
        });
}

function removeLike(contentId, contentType, userId) {
    let likeKey = `${likeKeyBase}-${contentType}-${contentId}`;

    localStorage.removeItem(likeKey);

    const request = {
        cache: "no-cache",
        method: "POST",
        body: JSON.stringify({
            contentId: contentId,
            contentType: contentType,
            userId: userId
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    };

    fetch(removeLikeUrl, request)
        .then((res) => {
            return res.json();
        })
        .then(data => {
            console.log({data});
        });
}

function tryGetLike(contentId, contentType) {
    let likeKey = `${likeKeyBase}-${contentType}-${contentId}`;
    let isLiked = localStorage.getItem(likeKey);

    return isLiked !== null;
}

async function requestLikes(contentId, contentType) {
    const request = {
        cache: "no-cache",
        method: "POST",
        body: JSON.stringify({
            contentId: contentId,
            contentType: contentType
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    };

    const totalLikes = await fetch(getTotalLikesUrl, request)
        .then((res) => {
            return res.json();
        })
        .then(data => {
            return data.result;
        });

    return totalLikes;
}

export {
    addLike,
    removeLike,
    tryGetLike,
    requestLikes,
    likeKeyBase
}